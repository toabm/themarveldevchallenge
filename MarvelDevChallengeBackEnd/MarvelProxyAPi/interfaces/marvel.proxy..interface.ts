export interface MarvelProxyInterface {
    getCharacters: (query: string) => Promise<any>;
}