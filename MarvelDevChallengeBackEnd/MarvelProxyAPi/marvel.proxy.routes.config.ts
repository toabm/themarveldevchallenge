import { CommonRoutesConfig } from '../common/common.routes.config';
import MarvelProxyController from './controllers/marvel.proxy.controller';
import express from 'express';

// The body() method will validate fields and generate an errors list�stored in the express.
import { body } from 'express-validator';
// We then need our own middleware to check and make use of the errors list.
import BodyValidationMiddleware from '../common/middleware/body.validation.middleware';


export class MarvelProxyRoutesConfigRoutes extends CommonRoutesConfig {

    apiPath: string = '';

    constructor(app: express.Application) {
        super(app, 'MarvelProxyRoutes');
    }

    configureRoutes(): express.Application {

        // Set API module fot ChuckNorris routes.
        this.apiPath =  `${this.baseURL}/marvelproxy`;

        // The API's URL http://localhost:3000/api/v1 will just inform that the API Server is up and running.
        this.app
            .route(`${this.baseURL}`).get(CommonRoutesConfig.apiWorking);

        // http://localhost:3000/api/v1/marvelproxy/characters --> Get marvel characters
        this.app
            .route(`${this.apiPath}/characters`)
            .get(MarvelProxyController.getCharacters)

        return this.app;
    }
}

