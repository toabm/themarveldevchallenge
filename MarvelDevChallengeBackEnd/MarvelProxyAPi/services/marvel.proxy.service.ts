import marvelProxyDAO from '../dao/marvel.proxy.dao';
import { MarvelProxyInterface } from '../interfaces/marvel.proxy..interface';

/**
 * MarvelProxy  Service class
 */
class MarvelProxyService implements MarvelProxyInterface {
    // Get a random Chuck Norris Joke from API Server
    getCharacters(query: string): Promise<any> {return marvelProxyDAO.getCharacters(query)}
}
export default new MarvelProxyService();