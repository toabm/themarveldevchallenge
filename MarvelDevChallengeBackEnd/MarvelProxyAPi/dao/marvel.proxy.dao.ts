// Promise based HTTP client for the browser and node.js
import axios from "axios";
// Import connection to MongoDB.
import debug from 'debug';
// We will feed data from external API:
const BASE_URL = `https://gateway.marvel.com/v1/public`;

const log: debug.IDebugger = debug('app:marvelproxy-dao');

class MarvelProxyDAO {

    constructor() {
        log('Created new instance of MarvelProxyDAO');
    }

    async getCharacters(query: string) {
        console.log(`${BASE_URL}/characters?${query}`);
        let response = await axios.get(`${BASE_URL}/characters?${query}`).then( (data) => {return data.data});
        console.log(response);
        return;
    }
}
// Using the singleton pattern, this class will always provide the same instance
export default new MarvelProxyDAO();