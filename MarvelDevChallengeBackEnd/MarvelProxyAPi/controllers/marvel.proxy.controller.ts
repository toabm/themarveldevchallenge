/**
 * The idea behind controllers is to separate the route configuration from the code that finally processes a route request.
 *
 * That means that all validations should be done before our request reaches the controller. The controller only needs to
 * know what to do with the actual request because if the request made it that far, then we know it turned out to be valid.
 * The controller will then call the respective service of each request that it will be handling.
 */

// We import express to add types to the request/response objects from our controller functions
import express from 'express';
// We import our newly created user services
import marvelProxyService from '../services/marvel.proxy.service';


// We use debug with a custom context
import debug from 'debug';

const log: debug.IDebugger = debug('app:marvelproxy-controller');

class MarvelProxyController {


    constructor() {
    }

    /**
     * Get characters.
     *
     * The request may include a query param "nameStartsWith" to search for characters starting with corresponding letter.
     * @param req
     * @param res
     */
    async getCharacters(req: express.Request, res: express.Response) {
        const response = await marvelProxyService.getCharacters(req.originalUrl.split("?")[1]);
        res.status(200).send(response);
    }




}

export default new MarvelProxyController();