/**
 * DTO contains just the fields that we want to pass between the API client and our database.
 */
export interface AnalyticsDto {
    // id: string; // Mongoose automatically makes an _id field available
    heroId: string,
    heroName: string,
    date: Date,
}