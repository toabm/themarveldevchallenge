/**
 * The idea behind controllers is to separate the route configuration from the code that finally processes a route request.
 *
 * That means that all validations should be done before our request reaches the controller. The controller only needs to
 * know what to do with the actual request because if the request made it that far, then we know it turned out to be valid.
 * The controller will then call the respective service of each request that it will be handling.
 */

// We import express to add types to the request/response objects from our controller functions
import express from 'express';
// We import our newly created user services
import analyticsService from '../services/analytics.service';


// We use debug with a custom context
import debug from 'debug';

const log: debug.IDebugger = debug('app:analytics-controller');

class AnalyticsController {

    /**
     * Get list of searchs.
     *
     * The list will be returned as a HTML table so it can be read in a html explorer.
     * @param req
     * @param res
     */
    getAnalytics = async (req: express.Request, res: express.Response) => {
        const response = await analyticsService.getAnalytics(100, 0);
        res.status(200).send(this.getTableTemplate(response));
    }


    /**
     * Save search on ddbb.
     * @param req
     * @param res
     */
    async saveAnalytics(req: express.Request, res: express.Response) {
        const result = await analyticsService.saveAnalytics(req.body);
        res.status(200).send(result);
    }





    /**
     * This method formats the results from listing all searchs stored in ddbb (/api/v1/searchs) in an HTML table.
     * @param analytics
     * @private
     */
    private getTableTemplate(analytics: []) {
        return `<html><head><meta charset="UTF-8"><style>
                        table, th, td {
                            border: 1px solid black;
                            text-align: center; }
                        li {text-align: justify;}
                        li:nth-child(2n) {background-color: lightcyan }
                    </style></head><body>
                    <table style="border: 2px solid grey">
                        <thead><tr style="border: 2px solid grey"><td>B�squeda</td><td>Total</td><td>Resultados</td></tr></thead>
                        <tbody>${analytics.map((r:any) => `<tr style="border: 2px solid grey"><td>${r.search}</td><td>${r.total}</td>
                                <td><ul>${r.result.map((entry:any) => `<li>${JSON.stringify(entry)}</li>`).join("")}</ul></td>
                            </tr>`).join("")}
                        </tbody>
                    </table></body></html><`
    }


}

export default new AnalyticsController();