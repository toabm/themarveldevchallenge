export interface analyticsInterface {
    getAnalytics: (limit: number, page: number) => Promise<any>;
    saveAnalytics: (resource: any) => Promise<any>;
}