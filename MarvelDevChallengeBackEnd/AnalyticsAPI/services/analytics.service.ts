import SearchsDao from '../dao/analytics.dao';
import { analyticsInterface} from '../interfaces/analytics.interface';




/**
 * Users Service class
 *
 * We�re using updateUserById() for both PUT and PATCH
 */
class AnalyticsService implements analyticsInterface {




    async getAnalytics(limit: number, page: number): Promise<any> {
        return SearchsDao.getAnalytics()
    }

    saveAnalytics(resource: any): Promise<any> {return SearchsDao.saveAnalytics(resource);}

    sendEmail(resource: any): Promise<any> {return SearchsDao.sendEmail()}

}
export default new AnalyticsService();