import { CommonRoutesConfig } from '../common/common.routes.config';
import AnalyticsController from './controllers/analytics.controller';
import express from 'express';
// The body() method will validate fields and generate an errors list�stored in the express.
import { body } from 'express-validator';
// We then need our own middleware to check and make use of the errors list.
import BodyValidationMiddleware from '../common/middleware/body.validation.middleware';

export class AnalyticsRoutesConfigRoutes extends CommonRoutesConfig {

    apiPath: string = '';

    constructor(app: express.Application) {
        super(app, 'AnalyticsRoutes');
    }

    configureRoutes(): express.Application {

        // Set API module fot Analytcs routes.
        this.apiPath =  `${this.baseURL}/analytics`;

        this.app
            .route(`${this.apiPath}`)
            .get(AnalyticsController.getAnalytics)
            .post(AnalyticsController.saveAnalytics)

        return this.app;
    }
}

