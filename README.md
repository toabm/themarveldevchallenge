> This is my proposed solution for the code challenge proposed by AcidTango that you can find on this repo under
>> [Marvel DEV Challenge](MarvelCodeChallenge.pdf)

# MarvelDevChallengeFrontEnd
 
> Navigate to [MarvelDevChallengeFrontEnd](MarvelDevChallengeFrontEnd) front end folder for further explanations.


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

This application unes Marvel Developer's API to show all Marvel heroes. 
It uses pagination on both front and server side to increment the app's performance. The page side is set to 9. so every request will return 
just the 9 heroes corresponding to the page.
It also implements filtering heroes by its staring letter. For instance, clicking the letter 'A' you will be able to navigate throw all heroes 
starting with 'A'.


# MarvelDevChallengeBackEnd



> Navigate to [MarvelDevChallengeBackEnd](MarvelDevChallengeBackEnd) front end folder for further explanations. Currently if you want to see the back-end code you have to switch to develoo_backend branch

This module is under construction. 

+ It acts as a proxy server between our FrontEnd app and the actual Marvel Server allowing us for example to use same credentials from several apps.

+ It also implements an Analytics API to store results on a database that wen will be able to analyze later on, such as number of clicks for each hero.

+ It also acts as a static files server, serving the files for the FrontEnd app when requested from browser.

+ It is dockerized, and make uses of Docker Compose to create a container stored on Docker Hub cloud to facilitate server implementations.