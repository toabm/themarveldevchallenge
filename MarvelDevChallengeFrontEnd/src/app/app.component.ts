import {Component, HostBinding} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MarvelDevChallengeFrontEnd';


  @HostBinding("class.drawer-open") isDrawerOpen: boolean = false;

  /**
   *
   * @param isDrawerOpen
   */
  toggleDrawer(isDrawerOpen: boolean) {
    this.isDrawerOpen = isDrawerOpen;
  }


}
