import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ConfigService} from "../../services/config.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  /**
   * Class constructor
   * @param router
   * @param config
   */
  constructor( private router: Router, private config: ConfigService) { }

  ngAfterViewInit(): void {
    console.log('RECEIVED CONFIGURATION:');
    console.log(this.config.appConfigValues);
  }

  ngOnInit(): void {
  }



  /**
   * Use AppRoutingModule to navigate app routes
   */
  goTo(section: string): void {this.router.navigateByUrl(section)}

}
