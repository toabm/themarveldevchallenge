import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AZNavBarComponent } from './aznav-bar.component';

describe('AZNavBarComponent', () => {
  let component: AZNavBarComponent;
  let fixture: ComponentFixture<AZNavBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AZNavBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AZNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
