import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-aznav-bar',
  templateUrl: './aznav-bar.component.html',
  styleUrls: ['./aznav-bar.component.scss']
})
export class AZNavBarComponent implements OnInit {

  alphabet: string[];
  clickedVowel: string = '☉';
  @Output("vowelClicked") vowelClicked: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
    // Get the alphabet: A to Z
    const alpha = Array.from(Array(26)).map((e, i) => i + 65);
    this.alphabet = alpha.map((x) => String.fromCharCode(x));
    this.alphabet.unshift('☉'); // Add AllCharacters symbol at the begining of the list.
  }

  ngOnInit(): void {}

  onCharClicked(c: string) {
    this.vowelClicked.emit(c == '☉' ? '' : c); // If ALL was clicked we will emit ''. Else we emit clicked char.
    this.clickedVowel = c;
  }



}
