import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable, tap, map} from "rxjs";
import {CharacterService} from "../../services/character.service";


@Component({
  selector: 'app-char-list',
  templateUrl: './char-list.component.html',
  styleUrls: ['./char-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CharListComponent implements OnInit {

  // List of retrieved characters.
  public characters: any[] = [];
  asyncChars!: Observable<any>;

  public selectedCharId:string = '';
  public clickedVowel: string = '';

  // Pagination vars.
  pageNumber: number = 1;
  total: number = 0;
  loading: boolean = true;
  itemsPerPAge: number = 9;


  constructor( private characterService: CharacterService) { }

  ngOnInit(): void {
    this.getCharactersPage(1);
  }

  /**
   * Will retrieve characters using CharacterService.
   * @param page NUmber of the page we want to retrieve
   * @param nameStartsWith  If we are searching characters by its start letter (click on app-aznav-bar)
   */
  getCharactersPage(page: number, nameStartsWith: string = '') {
    this.loading = true;
    this.selectedCharId = ''
    this.asyncChars = this.characterService.getPaginatedCharacters(page, this.itemsPerPAge, nameStartsWith).pipe(
      tap(res => {
        this.total = res.data.total;
        this.pageNumber = page;
        this.loading = false;
      }),
      map(res => res.data.results)
    );
  }

  onCharacterClicked($event: MouseEvent) {
    let target = ($event.target || $event.srcElement || $event.currentTarget) as Element;
    let targetId = target.getAttribute("data-char-id");

    this.selectedCharId != targetId
      ? this.selectedCharId = targetId || ''
      : this.selectedCharId = '';
  }

  onVowelClicked(vowel: string){
    this.clickedVowel = vowel;
    this.getCharactersPage(1, vowel);
  }

}

