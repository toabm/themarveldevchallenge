import {APP_INITIALIZER, NgModule, OnInit} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NavDrawerComponent} from "./components/nav-bar/nav-drawer/nav-drawer.component";
import {NavBarComponent} from "./components/nav-bar/nav-bar.component";
import {RouterModule} from "@angular/router";
import { HomeComponent } from './components/home/home.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {ConfigService} from "./services/config.service";
import {Observable, lastValueFrom} from "rxjs";
import {AppConfigurationInterface} from "./models/config";
import '../config.json';
import { CharListComponent } from './components/char-list/char-list.component';
import {FormsModule} from "@angular/forms";
import {NgxPaginationModule} from "ngx-pagination";
import { AZNavBarComponent } from './components/aznav-bar/aznav-bar.component';
import {AuthorizationInterceptor} from "./api_connect/authorization.interceptor";
import { UnderConstructionComponent } from './components/under-construction/under-construction.component';

/**
 * This is a function that returns a function that returns a Promise<boolean>.
 * The promise function loads your configuration information and stores it in your application.
 * Once your configuration has been loaded, you resolve the promise using resolve(true).
 *
 * This method will load configuration parameters following this priority:
 *  + It will first retrieve parameters from config.json file --> This file must be edited in deployment with clients parameters (API URL).
 *  + It will then retrieve parameters from server --> Property apiConfig.API_ENDPOINT must have been configured on previous step.
 *  + All the parameter not found in the previous steps will be loaded from Constants.defaultConfig
 */
function loadConfig(http: HttpClient, appConfigurationService: ConfigService): (() => Promise<boolean>) {
  return (): Promise<boolean> => {
    return new Promise<boolean>(( resolve: (a: boolean) => void, reject: (a: boolean) => void) => {

      // lastValueFrom takes as input an Observable and returns a Promise with the last emitted value, so we can use
      // promises' then() method to concat the async process and take the configuration properties in the correct order:
      // First we get values from config.json -> Once completed we retrieve values from database.
      lastValueFrom(http.get('./config.json') as Observable<AppConfigurationInterface>)
        .then((receivedConfig: AppConfigurationInterface) =>
            appConfigurationService.mapConfigValues(receivedConfig as AppConfigurationInterface),
          (err: Error) => alert("Config not loaded, can't continue..."))
        // .then(() =>
        //   lastValueFrom(appConfigurationService.getServerAppConfiguration()))
        // .then((receivedConfig) =>
        //   appConfigurationService.mapConfigValues(receivedConfig as AppConfigurationInterface))
        .then(() =>
          resolve(true)); // After loading the 3 configuration sources we resolve the returned promise.

    });
  };
}

@NgModule({
  declarations: [AppComponent, NavBarComponent, NavDrawerComponent, HomeComponent, CharListComponent, AZNavBarComponent, UnderConstructionComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, RouterModule, FormsModule, NgxPaginationModule],
  providers: [
    { // APP_INITIALIZER is a multi provider type that lets you specify a factory that returns a promise. When the promise completes, the application will continue on.
      provide: APP_INITIALIZER,
      // useFactory points to a function that returns a function that returns a Promise<boolean>
      useFactory: loadConfig,
      // This is because APP_INITIALIZER allows multiple instances of this provider. They all run simultaneously, but the code will not continue beyond APP_INTITIALIZER until all of the Promises have resolved.
      multi: true,
      // Configures the Injector to return an instance of a token.
      deps: [HttpClient, ConfigService]
    },
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule implements OnInit {
  constructor() {}
  ngOnInit() {}
}
