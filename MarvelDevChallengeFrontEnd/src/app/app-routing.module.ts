/**
 * ROUTING MODULE
 *
 * Generated by following command:
 *  $ ng generate module app-routing --flat --module=app
 *    --flat puts the file in src/app instead of its own folder.
 *    --module=app tells the CLI to register it in the imports array of the AppModule.
 *
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {CharListComponent} from "./components/char-list/char-list.component";
import {UnderConstructionComponent} from "./components/under-construction/under-construction.component";




const routes: Routes = [
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // ALBUMS ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  {
    path: '',
    component: HomeComponent,
    data: { title: 'Home' }
  },
  {
    path: 'random',
    component: UnderConstructionComponent,
    data: { title: 'Random Character' }
  },

  {
    path: 'charList',
    component: CharListComponent,
    data: { title: 'Choose Character' }
  },

  {
    path: 'search',
    component: UnderConstructionComponent,
    data: { title: 'Search Character' }
  },

  {
    path: '',
    component: HomeComponent,
    data: { title: 'Album List' }
  },


];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
