// Angular Modules
import { Injectable } from '@angular/core';
// Application Constants
import {environment} from "../../environments/environment";
import {ConfigService} from "../services/config.service";

/**
 * This service can:
 *   + Create an API URL that uses the real or the mock API.
 *   + Create an API URL with query strings.
 *   + Create an API URL with path variables.
 *
 * All the API URLs will be provided from this service.
 */
@Injectable({providedIn: 'root'})export class ApiEndpointsService {

  constructor(private appConfig: ConfigService) {}

  /**
   * CREATE URL: API URL IS CONFIGURED HERE FROM CONFIG.JSON
   */

  private createUrl(action: string): string {
    return [ !environment.production
      //? this.appConfig.appConfigValues!.apiConfig.API_MOCK_ENDPOINT
      ? this.appConfig.appConfigValues!.apiConfig.API_ENDPOINT
      : this.appConfig.appConfigValues!.apiConfig.API_ENDPOINT, action].join('/');
  }


  // CHARACTERS
  public getCharactersEndpoint(): string {
    return this.createUrl(`characters`);
  }


}
