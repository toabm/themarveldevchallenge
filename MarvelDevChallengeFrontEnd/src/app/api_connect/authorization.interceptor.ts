/**
 * The JWT Interceptor intercepts http requests from the application to add a JWT auth token to the Authorization header
 * if the user is logged in.
 *
 * It's implemented using the HttpInterceptor class that was introduced in Angular 4.3 as part of the new HttpClientModule.
 * By extending the HttpInterceptor class you can create a custom interceptor to modify http requests before they get sent to
 * the server.
 *
 * Http interceptors are added to the request pipeline in the providers section of the app.module.ts file.
 */
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ConfigService} from "../services/config.service";

/**
 * This interceptor will add authorization query params on URL for every http request our app will make.
 */
@Injectable({providedIn: 'root'})
export class AuthorizationInterceptor implements HttpInterceptor {

  // Paramos needed for API authorization.
  authorizationParams: {apikey: string, ts: number} = {
    apikey: this.config.appConfigValues?.apiConfig.API_PUBLIC_KEY!,
    ts: new Date().getTime()
  }

  constructor(private config: ConfigService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const cloneReq = request.clone({
      setParams: {apikey: this.config.appConfigValues?.apiConfig.API_PUBLIC_KEY!, ts: new Date().getTime().toString()}
    })
    return next.handle(cloneReq)
  }
}
