import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import {AppConfigurationInterface, mapConfig} from "../models/config";
import {ApiHttpService} from "../api_connect/api-http.service";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpClient,
              private apiHttpService: ApiHttpService) { }

  // Init config with DEFAULT CONFIG values
  appConfigValues: AppConfigurationInterface | undefined;

  /**
   * Get configuration object from database. API_CONFIG_ENDPOINT must be previously configured.
   *
   * The config parameters will be requested to a separate API.
   */
  getServerAppConfiguration(): Observable<AppConfigurationInterface> {
    return this.apiHttpService.get(this.appConfigValues!.apiConfig.API_CONFIG_ENDPOINT + "/appConfiguration")
  }

  /**
   * This method will save a set of AppConfigurationInterface values to this.appConfigValues. Only the properties contained
   * by receivedConfig that already exists within this.appConfigValues will be saved, any other property will be ignored.
   * @param receivedConfig
   */
  mapConfigValues(receivedConfig: AppConfigurationInterface) {
    return new Promise((resolve, reject) => {
      this.appConfigValues = mapConfig(receivedConfig);
      resolve(this.appConfigValues)
    });
  }


}
