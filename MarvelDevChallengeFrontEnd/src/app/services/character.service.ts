import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {IServerResponse} from "../models/iServerResponse";
import {HttpClient, HttpParams} from "@angular/common/http";
import {ApiEndpointsService} from "../api_connect/api-endpoints.service";

// Params needed for paginated request to API.
export type PaginationParams = {limit: number, offset: number}

@Injectable({providedIn: 'root'})
export class CharacterService {


  constructor(private http: HttpClient,
              private apiEndpoints: ApiEndpointsService) { }


  getPaginatedCharacters(page: number, itemsPerPAge: number = 9, nameStartsWith: string = ''): Observable<IServerResponse> {
    const startRecord = (page - 1) * itemsPerPAge;
    // const endRecord = startRecord + perPage;

    let params: PaginationParams = {
      limit: itemsPerPAge,
      offset: startRecord
    };
    // Add nameStartsWith to search params in case nameStartsWith != ''
    if (nameStartsWith) params = {...params, ...{nameStartsWith: nameStartsWith}}
    return this.getCharactersWithParams(params);
  }


  public getCharactersWithParams(params: PaginationParams): Observable<any> {
    return this.http.get<IServerResponse>(this.apiEndpoints.getCharactersEndpoint(),
      { params: new HttpParams({ fromObject: params}) });
  }


}
