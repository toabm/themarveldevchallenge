/** Types definitions for configuration objects: Config parameters are grouped by types */

type ApiConfig = {
  [key: string]: string | undefined
  API_ENDPOINT: string        // URL for production API
  API_PUBLIC_KEY: string,     // Public Key for Marvel API
  API_PRIVATE_KEY: string,    // Private Key for Marvel API

  SERVER_MARVEL_PROXY: string,  // URL for sending request throw our proxy server.
  SERVER_ANALYTICS: string,

  API_MOCK_ENDPOINT: string   // URL for demo API
  API_CONFIG_ENDPOINT: string // URL for our own Configuration Endpoint
}

/**
 * Configuration object.
 */
export interface AppConfigurationInterface {
  [key: string]: any;
  apiConfig: ApiConfig
}


const emptyAppConfig = (): AppConfigurationInterface => ({
  apiConfig: {
    API_ENDPOINT: '',
    API_PUBLIC_KEY: '',
    API_PRIVATE_KEY: '',
    SERVER_MARVEL_PROXY: '',
    SERVER_ANALYTICS: '',
    API_MOCK_ENDPOINT: '',
    API_CONFIG_ENDPOINT: ''
  }
});

export const mapConfig = <T extends Partial<AppConfigurationInterface>>(initialValues: T): AppConfigurationInterface & T => {
  return Object.assign(emptyAppConfig(), initialValues);
};
