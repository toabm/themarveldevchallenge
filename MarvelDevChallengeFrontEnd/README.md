# MarvelDevChallengeFrontEnd

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

This application unes Marvel Developer's API to show all Marvel heroes.
It uses pagination on both front and server side to increment the app's performance. The page side is set to 9. so every request will return
just the 9 heroes corresponding to the page.
It also implements filtering heroes by its staring letter. For instance, clicking the letter 'A' you will be able to navigate throw all heroes
starting with 'A'.

![App ScreenShot](../MarvelDevChallengeFrontEnd/src/assets/imgs/app_UI_sample.png)*The front App showing CharList Component*


## Let's explain the angular components used to compose the UX:

+ NavBar Component: The nav-bar allowing us to navigate from HOME, to HEROES section, and 2 non existing sections that will show the Under Construction page. It makes use of a drawer to implemente responsiveness.
+ Under Construction Component: Self explained...:)
+ Home Component: IDEM
+ CharList Component: IT is the main component. It will show a paginated grid with 9 heroes in each page. ngx-pagination module has been used for pagination. Every page change will generate a request. On click on a hero, it will be zoomed in to show further information.
+ AZNAvBar Component: A vertical bar showing the alphabet that will retrieve heroes from server filtered by its starting letter when any of the letter is clicked.

Besides this componentes for the interface, the following modules are underneath:

## API CONNECT:
+ API Service: Is the lowest layer to connect with the API. It only defines the generic method allowed by the API: get, post...
+ Authorization Interceptor: This in an Angular HTTP Interceptor that will add the authorization query params needed for the Marvel API to any request made from front-end
+ API Endpoints Service: It centralizes generation of API URL.

## MODELS
+ IServerResponse: I have only included this general model that will implement the structure of the server response. The heroes will come under data.results array.
+ Config Model: Model to save app configuration. Config will be explained further under Configuration Service.

## SERVICES
+ Config Service: This app make uses of HTTP_INTERCEPTORS to assure configuration params to be loaded before the apps starts. Config params will be saved in config.json file so configuration changes can be made without having to deploy again. Config file stores only the API URLs currently. The Config Service will store the config object wna will be accesible by injection from the rest of the app.
+ Character Service: It allows us to communicate with MarvelAPI for everything regarding characters. It actually only implements 2 methods: One for retrieving all chars, the second one to retrieve characters using pagination capabilities. The latest is the one used by CharList Component.

## DEPLOYMENT

### PREREQUISITES

To be able to navigate the app you must have installed:

+ NPM >= 7
+ Node >= 14
+ Angular CLI >= 12

### Development server

You must first download the application code in the repo. Then navigate to MarvelDevChallengeFrontEnd folder and run
```` 
$ ng serve
````

for a development server. Then navigate to `http://localhost:4200/` in your browser (tested with Google Chrome). The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Problems
+ Problems with GIT: Added all un-versioned files to git and had to revert some commits remove them from git cache.
+ Asset config.json not being loaded in the right path --> Had to add configuration under angular.json
